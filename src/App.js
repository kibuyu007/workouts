import './App.css';
import Navbarr from './Components/Navbarr';
import { Routes, Route } from 'react-router-dom';
import Malipo from './Components/Malipo';
import Home from './Components/Home';


function App() {
  return (
    <div className="App">

      <Navbarr/>

      <div className='pages'>
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/Add' element={<Malipo />} />
      </Routes>
      </div>

    </div>
  );
}

export default App;
