import axios from 'axios';
import React, { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom';


const Home = () => {

  const postUrl = 'http://localhost:4000/create'

  const [data , setData] = useState({
    Name: '',
    Age: '',
    City: '',
    Country: ''
  });


  const onChange = (e)=> {
    const {name,value} = e.target;
    setData({...data,[name]:value});
  }


  let navigate = useNavigate();


  const onSubmitt = async (e)=> {

    e.preventDefault();

    const postData = {
      Name: data.Name,
      Age: data.Age,
      City: data.City,
      Country: data.Country
    }

    await axios.post(postUrl,postData);
    setData([...data,postData]);
    navigate('/Add');

  }


  useEffect(()=>{
    
  });



  return (
    <div className='Home'>
      <div className='box'>
        <div className='form'>
          <h2> Sajili Hapa</h2>

          <form onSubmit={onSubmitt}>

            <div className='inputBox'>
            <input 
              name='Name'
              type='text'
              placeholder='Name..'
              value={data.Name}
              onChange={onChange}
              required
              />
            </div>

            <div className='inputBox'>
            <input
             name='Age'
             type='number'
             placeholder='Age..'
             value={data.Age}
             onChange={onChange}
             required
             />
            </div>

            <div className='inputBox'>
            <input
             name='City'
             type='text'
             placeholder='City..'
             value={data.City}
             onChange={onChange}
             required
            />
            </div>

            <div className='inputBox'>
            <input
             name='Country'
             type='text'
             placeholder='Country..'
             value={data.Country}
             onChange={onChange}
             required
            />
            </div>

            <button className='button' > Save </button>
          </form>
        </div>

      </div>
    </div>
  )
}

export default Home