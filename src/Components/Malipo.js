import React from 'react'
import axios from 'axios'
import { useState, useEffect } from 'react'




const Malipo = () => {

  const getUrl = 'http://localhost:4000/get'

  const [list, setList] = useState([]);

 


  const getData = async () => {
    const { data: vutaData } = await axios.get(getUrl)
    setList(vutaData);
  }



  useEffect(() => {
    getData();
  }, []);




  return (
    <div className='Malipo'>
      <div className='malipos'>
        <h4> Number of Users is :  {list.length} </h4>

        <table className='table'>
          <thead>
            <tr>
              <th> SN </th>
              <th> Name </th>
              <th> Age </th>
              <th> City </th>
              <th> Update </th>
              <th> Remove </th>

            </tr>
          </thead>

          <tbody>
            {list.map((list) => {

              return (
                <tr key={list._id}>
                  <td> {list.Name} </td>
                  <td> {list.Age} </td>
                  <td> {list.City} </td>
                  <td> {list.Country} </td>
                  <td> <button  className='btn btn-info btn-sm'> Edit  </button> </td>
                  <td> <button  className='btn btn-danger bt-sm'> Remove </button> </td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>
    </div>
  )
}

export default Malipo