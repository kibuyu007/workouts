import React from 'react'
import { Link } from 'react-router-dom'

const Navbarr = () => {
    return (
        <nav className='NavbarCss'>
            <h3>
                NIDA
            </h3>

            <ul className='NavMenu'>
                <Link to="/" className='NavLink'> Ukurasa </Link>
                <Link to="/Add" className='NavLink'> Malipo </Link>
            </ul>

        </nav>
    )
}

export default Navbarr